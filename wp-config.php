<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

/** MySQL database username */

/** MySQL database password */

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
define('FS_METHOD', 'direct');
define('AUTH_KEY',         't&pjp@IE>tVuy6-5x(0sbS.}$,|OHtWGr^Ve@5|Lim&kM+qOz$fX%kL`ULzzOb_a');
define('SECURE_AUTH_KEY',  'Sy/6YXL+&gm8h@RU](!EN4-@z?[{}`)++A3PVwx@pw#-=R{N;+ZEw01V}X1-QL4&');
define('LOGGED_IN_KEY',    'm/VPr0%r]E=)(4NMz*OcAV`)sC7HMI@m/y%g?0}%*KEddJ)9s#1I 2gl`MQnyo%)');
define('NONCE_KEY',        '?007b6a|i#Tzt6MW.^Qm2VemV$+b8.Y+r(-`@(9c1yRWbb|e5(hXa-rB;YxbxJ2M');
define('AUTH_SALT',        'li,!P7XLY-:{A_C|F%M2Q=tR%64WU=iM8lr,N_t!,yk|T$, =8IFjx}S<Hm|]|4)');
define('SECURE_AUTH_SALT', '+uHG5{ 3xf2(^mNyepi#,t>s30lZ|*M $4fpsf<[fMzUN]t=*_<8dKicO_LEYl8[');
define('LOGGED_IN_SALT',   'o_u.ihh#E^4,W.Rf-|_ ||TsrHjU;_UKK09CmwvUL$c4YzX:OK]5B;XRnc!9!2kK');
define('NONCE_SALT',       '>$Q&]rw+w{LN@x=2J!ZXMz`7_+:W]B8{E$(=B;MNb%[KEA{)DNtl0E(m&YRR(2k,');
define('DB_NAME', 'wordpress');
define('DB_USER', 'wordpress');
define('DB_PASSWORD', 'RhEzahVTC0');
require_once(ABSPATH . 'wp-settings.php');
